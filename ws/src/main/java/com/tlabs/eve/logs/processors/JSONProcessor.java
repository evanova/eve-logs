package com.tlabs.eve.logs.processors;

import java.io.IOException;
import java.io.InputStream;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

public class JSONProcessor implements Processor {

    private static final String JSON;

    static {
        final InputStream in = JSONProcessor.class.getResourceAsStream("/message.json");
        if (null == in) {
            throw new IllegalArgumentException();
        }
        try {
            JSON = IOUtils.toString(in);
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        finally {
            IOUtils.closeQuietly(in);
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String body = exchange.getIn().getBody(String.class);
        if (StringUtils.isBlank(body)) {
            return;
        }

        final String newBody = String.format(
                JSON,
                exchange.getProperty(PropertyProcessor.PROPERTY_CHANNEL_ID),
                exchange.getProperty(PropertyProcessor.PROPERTY_CHANNEL_NAME),
                exchange.getProperty(PropertyProcessor.PROPERTY_CHANNEL_LISTENER),
                //date
                StringUtils.trim(StringUtils.substringBetween(body, "[", "]")),
                //name
                StringUtils.trim(StringUtils.substringBetween(body, "]", ">")),
                //text
                StringUtils.trim(StringUtils.substringAfter(body, ">"))
                );

        exchange.getIn().setBody(newBody);
    }
}
