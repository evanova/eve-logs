package com.tlabs.eve.logs.filters;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.commons.lang.StringUtils;

public class LineFilter implements Predicate {

    @Override
    public boolean matches(Exchange exchange) {
        String body = exchange.getIn().getBody(String.class).trim();
        if (StringUtils.isBlank(body)) {
            return false;
        }
        if (body.startsWith("--")) {
            return false;
        }
        return true;
    }
}
