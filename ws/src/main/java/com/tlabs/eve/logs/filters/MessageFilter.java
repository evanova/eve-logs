package com.tlabs.eve.logs.filters;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.commons.lang.StringUtils;

public class MessageFilter implements Predicate {
    
    @Override
    public boolean matches(Exchange exchange) {
        String body = exchange.getIn().getBody(String.class);
        final String date = StringUtils.substringBetween(body, "[", "]");
        if (StringUtils.isBlank(date)) {
            return false;
        }
        return true;
    }
}
