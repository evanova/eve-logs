package com.tlabs.eve.logs;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.net.InetAddress;

public class Main {

    public static void main(String... args) throws Exception {
        final Options options = new Options();
        options.addOption(
                Option.builder("h").
                        desc("Server bind address and port (192.168.1.1:9090)").
                        numberOfArgs(1).
                        required(false).
                        longOpt("host").
                        build());
        options.addOption(
                Option.builder("d").
                        desc("Eve log files directory location (usually HOME\\Documents\\Eve\\logs)").
                        numberOfArgs(1).
                        required(false).
                        longOpt("directory").
                        build());

        try {
            final CommandLine line = new DefaultParser().parse(options, args);
            main(line);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            usage(options);
            System.exit(1);
        }
    }

    private static void main(final CommandLine line) throws Exception {
        final String directory = line.getOptionValue("d", System.getProperty("user.home") + "\\Documents\\Eve\\logs");

        String host = line.getOptionValue("h", InetAddress.getLocalHost().getHostName());
        if (StringUtils.isBlank(host)) {
            throw new IllegalArgumentException("Empty host.");
        }

        int port = 9090;
        final String[] split = StringUtils.split(host, ":");
        switch (split.length) {
            case 1:
                break;
            case 2:
                host = split[0];
                port = Integer.parseInt(split[1]);
                break;
            default:
                throw new IllegalArgumentException("Invalid host:port '" + host + "'");
        }

        mainImpl(host, port, directory);
    }

    private static void mainImpl(final String host, final int port, final String directory) throws Exception {
        final File file = new File(directory);
        if (!file.exists()) {
            throw new IllegalArgumentException(file.getName() + " does not exist.");
        }

        final org.apache.camel.main.Main camel = new org.apache.camel.main.Main();
        camel.enableHangupSupport();

        final LogRoutes route = new LogRoutes(directory, host, port);
        camel.addRouteBuilder(route);
        camel.run();
    }

    private static void usage(final Options options) {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("main", options);
    }
}
