package com.tlabs.eve.logs;

import org.apache.camel.builder.RouteBuilder;
import com.tlabs.eve.logs.filters.EmptyFilter;
import com.tlabs.eve.logs.filters.LineFilter;
import com.tlabs.eve.logs.filters.MessageFilter;
import com.tlabs.eve.logs.processors.FileProcessor;
import com.tlabs.eve.logs.processors.JSONProcessor;

public class LogRoutes extends RouteBuilder {

    private static final String LOG_URI =
            "file:%1$s" +
                    "?charset=UTF-16LE" +
                    "&recursive=true" +
                    "&sortBy=file:modified" +
                    "&noop=true" +
                    "&idempotentKey=${file:name}-${file:size}" +
                    "&autoCreate=false&startingDirectoryMustExist=true";

    private static final String WS_URI = "websocket://%1$s:%2$s/eve/%3$s?sendToAll=true";
    private static final String WS_PING = "websocket://%1$s:%2$s/eve/ping";

    private final int port;
    private final String host;

    private final String rootDirectory;

    public LogRoutes(final String rootDirectory, String host, int port) {
        this.host = host;
        this.port = port;
        this.rootDirectory = rootDirectory;
    }

    @Override
    public void configure() throws Exception {
        from(String.format(WS_PING, this.host, this.port)).
                log("${body}").
                setBody().
                constant("pong").
                to(String.format(WS_PING, this.host, this.port));

        configure("Chatlogs", "chat");
    }

    private void configure(final String log, final String wsPath) throws Exception {
        final String direct = "direct:" + wsPath;

        from(direct).
            errorHandler(defaultErrorHandler().disableRedelivery()).
            split().tokenize("\n", 1).
            filter(new LineFilter()).
            filter(new MessageFilter()).
            process(new JSONProcessor()).
            filter(new EmptyFilter()).
            to("log:?level=DEBUG").
            to(String.format(WS_URI, this.host, this.port, wsPath));

        from(String.format(LOG_URI, this.rootDirectory + "/" + log)).
                errorHandler(defaultErrorHandler().disableRedelivery()).
                process(new FileProcessor(direct));

    }

}
