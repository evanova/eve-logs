package com.tlabs.eve.logs.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class FileProcessor implements Processor {
    public static final String PROPERTY_SEND = "FileProcessor.sendTo";

    private static class Topic {
        long lastModified;
        String filename;
        String uri;
    }

    private static final String STREAM_URI =
            "stream:file?fileName=%1$s&scanStream=true&encoding=UTF-16LE";

    private final Map<String, Topic> topics = new HashMap<>();

    private final String wsUri;

    public FileProcessor(final String wsUri) {
        super();
        this.wsUri = wsUri;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String uri = route(exchange);
        if (StringUtils.isBlank(uri)) {
            exchange.setProperty(PROPERTY_SEND, "log:?level=ERROR");
            exchange.getIn().setHeader(PROPERTY_SEND, "log:?level=ERROR");
        }
        else {
            exchange.setProperty(PROPERTY_SEND, uri);
            exchange.getIn().setHeader(PROPERTY_SEND, uri);
        }
    }

    private String route(final Exchange exchange) throws Exception {
        long modified = exchange.getIn().getHeader(Exchange.FILE_LAST_MODIFIED, Long.class);
        final String filename = exchange.getIn().getHeader("CamelFileAbsolutePath", String.class);

        final String topic = StringUtils.substringBefore(filename, "_");
        if (StringUtils.isBlank(topic)) {
            return null;
        }

        Topic t = this.topics.get(topic);
        if (null == t) {
            t = new Topic();
            t.lastModified = modified;
            t.filename = filename;
            t.uri = addRoute(exchange, t, wsUri);
            this.topics.put(topic, t);
            return t.uri;
        }

        if (modified < t.lastModified) {
            return t.uri;
        }
        if (StringUtils.equals(filename, t.filename)) {
            return t.uri;
        }

        removeRoute(exchange, t);
        t.lastModified = modified;
        t.filename = filename;
        t.uri = addRoute(exchange, t, wsUri);
        return t.uri;
    }

    private static String addRoute(final Exchange exchange, final Topic topic, final String wsUri) throws Exception {
        final String uri = String.format(STREAM_URI, topic.filename);
        exchange.getContext().addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from(uri).
                        routeId(topic.filename + ".id").
                        errorHandler(defaultErrorHandler().disableRedelivery()).
                        convertBodyTo(String.class, "UTF-8").
                        process(new PropertyProcessor()).to(wsUri);
            }
        });
        return uri;
    }

    private static void removeRoute(final Exchange exchange, final Topic topic) throws Exception {
        exchange.getContext().stopRoute(topic.filename + ".id");
        exchange.getContext().removeRoute(topic.filename + ".id");
    }
}
