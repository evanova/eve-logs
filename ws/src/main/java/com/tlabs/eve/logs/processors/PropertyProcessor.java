package com.tlabs.eve.logs.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;

public class PropertyProcessor implements Processor {

    static final String PROPERTY_CHANNEL_ID = "EveChannelID";
    static final String PROPERTY_CHANNEL_NAME = "EveChannelName";
    static final String PROPERTY_CHANNEL_LISTENER = "EveChannelListener";

    private String channelID;
    private String channelName;
    private String listener;

    @Override
    public void process(Exchange exchange) throws Exception {
        capture(exchange);
        set(exchange);
    }

    private void set(Exchange exchange) throws Exception {
        if (StringUtils.isNotBlank(this.channelID)) {
            exchange.setProperty(PROPERTY_CHANNEL_ID, this.channelID);
        }
        if (StringUtils.isNotBlank(this.channelName)) {
            exchange.setProperty(PROPERTY_CHANNEL_NAME, this.channelName);
        }
        if (StringUtils.isNotBlank(this.listener)) {
            exchange.setProperty(PROPERTY_CHANNEL_LISTENER, this.listener);
        }
    }

    private void capture(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class).trim();
        if (body.startsWith("Channel ID")) {
            this.channelID = toChannelID(StringUtils.substringAfter(body, ":").trim());
            return;
        }
        if (body.startsWith("Channel Name")) {
            this.channelName = StringUtils.substringAfter(body, ":").trim();
            return;
        }
        if (body.startsWith("Listener")) {
            this.listener = StringUtils.substringAfter(body, ":").trim();
            return;
        }
    }
    private static String toChannelID(final String channelId) {
        String r = channelId;
        if (!StringUtils.contains(channelId, "(")) {
            return r;
        }
        r = StringUtils.remove(StringUtils.remove(StringUtils.remove(r, "'"), ")"), "(");
        r = StringUtils.remove(r, ",");
        return r;
    }
}
