package com.tlabs.android.evelogs.ui;

import com.tlabs.android.evelogs.Log;
import com.tlabs.android.evelogs.data.Channel;
import com.tlabs.android.evelogs.data.ChatData;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public final class ChatChannels {
    private final Subject<ChatData, ChatData> subject = BehaviorSubject.create();

    private final List<Channel> channels = new LinkedList<>();
    private final List<ChatData> all = new LinkedList<>();

    private final Map<String, List<ChatData>> messages = new HashMap<>();

    private final ChannelsAdapter channelsAdapter;
    private final MessagesAdapter allAdapter;

    private final Map<String, MessagesAdapter> messagesAdapter;
    public ChatChannels() {
        this.channelsAdapter = new ChannelsAdapter(this.channels);
        this.messagesAdapter = new HashMap<>();
        this.allAdapter = new MessagesAdapter(all);
    }

    public Observable<ChatData> observable() {
        return subject.
                subscribeOn(AndroidSchedulers.mainThread()).
                observeOn(AndroidSchedulers.mainThread());
    }

    public ChannelsAdapter getChannelsAdapter() {
        return this.channelsAdapter;
    }

    public MessagesAdapter getMessagesAdapter() {
        return this.allAdapter;
    }

    public MessagesAdapter getMessagesAdapter(final String channelId) {
        return this.messagesAdapter.get(channelId);
    }

    public List<Channel> getChannels() {
        final List<Channel> channels = new ArrayList<>(this.channels.size());
        channels.addAll(this.channels);
        return Collections.unmodifiableList(channels);
    }

    public int getMessageCount(final String channelId) {
        if (StringUtils.isBlank(channelId)) {
            return 0;
        }
        final List<ChatData> messages = this.messages.get(channelId);
        return (null == messages) ? 0 : messages.size();
    }

    public void onReceive(final ChatData data) {
        if (null == data) {
            return;
        }
        if (null == data.getChannel()) {
            return;
        }

        addChannel(data);

        Log.e(ToStringBuilder.reflectionToString(data.getChannel()));
        if (null != data.getMessage()) {
            addMessage(data);
        }

        channelsAdapter.notifyDataReceived(data);
        subject.onNext(data);
    }

    private void addChannel(final ChatData data) {
        for (Channel c: this.channels) {
            if (c.getId().equals(data.getChannel().getId())) {
                return;
            }
        }
        channels.add(data.getChannel());
        channelsAdapter.notifyItemInserted(this.channels.size() - 1);
    }

    private void addMessage(final ChatData data) {

        List<ChatData> messages = this.messages.get(data.getChannel().getId());
        if (null == messages) {
            messages = new LinkedList<>();
            this.messages.put(data.getChannel().getId(), messages);
            this.messagesAdapter.put(data.getChannel().getId(), new MessagesAdapter(data.getChannel(), messages));
        }

        messages.add(data);

        final MessagesAdapter adapter = this.messagesAdapter.get(data.getChannel().getId());
        adapter.notifyItemInserted(messages.size() - 1);

        this.all.add(data);
        this.allAdapter.notifyItemInserted(this.all.size() - 1);
    }
}
