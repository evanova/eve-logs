package com.tlabs.android.evelogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tlabs.android.evelogs.data.ChatData;
import com.tlabs.eve.logs.client.WebSocketObservable;

import org.apache.commons.lang.StringUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Observable;

@Module
public class ChatLogModule {
    private static final String KEY_HOST = "com.tlabs.android.evelogs.preferences.host";
    private static final String URI = "ws://%1$s/eve/%2$s";

    private final SharedPreferences.OnSharedPreferenceChangeListener preferencesListener = new SharedPreferences.OnSharedPreferenceChangeListener() {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (KEY_HOST.equals(key)) {
                setNewHost(sharedPreferences.getString(KEY_HOST, "192.168.1.1:9090"));
            }
        }
    };

    private final Context context;
    private final WebSocketObservable<ChatData> wsChat;
    private final WebSocketObservable<String> wsPing;

    private String host;

    public ChatLogModule(final Context context) {
        this.context = context.getApplicationContext();

        this.wsChat = new WebSocketObservable<>(ChatData.class);
        this.wsPing = new WebSocketObservable<>(String.class);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        prefs.registerOnSharedPreferenceChangeListener(preferencesListener);
        setNewHost(prefs.getString(KEY_HOST, "192.168.1.1:9090"));
    }

    @Provides
    @Singleton
    public Observable<ChatData> provideChatData() {
        return this.wsChat.getObservable();
    }

    @Provides
    @Singleton
    public Observable<String> providePingData() {
        return this.wsPing.getObservable();
    }

    private void setNewHost(final String newHost) {
        if (StringUtils.isBlank(newHost)) {
            return;
        }
        if (StringUtils.equals(this.host, newHost)) {
            return;
        }

        this.host = newHost;
        this.wsChat.setUri(String.format(URI, newHost, "chat"));
        this.wsPing.setUri(String.format(URI, newHost, "ping"));
        Log.d("Changed to " + this.host);
    }
}
