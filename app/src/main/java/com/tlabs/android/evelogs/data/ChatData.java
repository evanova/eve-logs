package com.tlabs.android.evelogs.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*{
  "channel": {
    "id": "2100446750",
    "name": "TheCitadel",
    "listener": "Tawa Kea"
  },
  "message": {
    "date": "2015.09.27 14:46:32",
    "name": "Xabib Elder",
    "text": "camping gates"
  }
}
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatData {

    @JsonProperty
    private com.tlabs.android.evelogs.data.Channel channel;

    @JsonProperty
    private com.tlabs.android.evelogs.data.Message message;

    public Channel getChannel() {
        return channel;
    }

    public com.tlabs.android.evelogs.data.Message getMessage() {
        return message;
    }
}
