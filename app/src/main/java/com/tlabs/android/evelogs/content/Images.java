package com.tlabs.android.evelogs.content;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

public class Images {
    private static final String ICON_CHARACTER = "%s/Character/%s_128.jpg";

    private static final String ICON_CORPORATION = "%s/Corporation/%s_128.png";

    private static final String ICON_ALLIANCE = "%s/Alliance/%s_128.png";

    public static final class CircleTransform extends BitmapTransformation {
        private final int borderWidth;
        private final int borderColor;

        public CircleTransform(final Context context) {
            this(context, 0, 0);
        }

        public CircleTransform(final Context context, final int borderWidth, final int borderColor) {
            super(context);
            this.borderWidth = borderWidth;
            this.borderColor = borderColor;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap source, int outWidth, int outHeight) {
            final int width = outWidth + borderWidth;
            final int height = outHeight + borderWidth;

            Bitmap canvasBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            BitmapShader shader = new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(shader);

            Canvas canvas = new Canvas(canvasBitmap);
            float radius = width > height ? ((float) height) / 2f : ((float) width) / 2f;
            canvas.drawCircle(width / 2, height / 2, radius, paint);

            //border code
            if (borderWidth > 0) {
                paint.setShader(null);
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(borderColor);
                paint.setStrokeWidth(borderWidth);
                canvas.drawCircle(width / 2, height / 2, radius - borderWidth / 2, paint);
            }

            if (canvasBitmap != source) {
                source.recycle();
            }

            return canvasBitmap;
        }

        @Override
        public String getId() {
            return "CircleTransform/" + borderWidth + "/" + borderColor;
        }
    }

    public static void loadCharacterIcon(final String ownerId, final ImageView intoView) {
        loadCharacterIcon(intoView.getContext(), ownerId, intoView);
    }

    private static void loadCharacterIcon(final Context context, final String ownerId, final ImageView... intoViews) {
        BitmapRequestBuilder c = iconify(context, formatUrl(ICON_CHARACTER, ownerId));
        for (ImageView v : intoViews) {
            c.into(v);
        }
    }

    public static void loadCorporationIcon(final String ownerId, final ImageView intoView) {
        loadCorporationIcon(intoView.getContext(), ownerId, intoView);
    }

    private static void loadCorporationIcon(final Context context, final String corpID, final ImageView... intoViews) {
        BitmapRequestBuilder c = iconify(context, formatUrl(ICON_CORPORATION, corpID));
        for (ImageView v : intoViews) {
            c.into(v);
        }
    }

    public static void loadAllianceIcon(final String ownerId, final ImageView intoView) {
        loadAllianceIcon(intoView.getContext(), ownerId, intoView);
    }

    private static void loadAllianceIcon(final Context context, final String allianceID, final ImageView... intoViews) {
        final BitmapRequestBuilder c = iconify(context, formatUrl(ICON_ALLIANCE, allianceID));
        for (ImageView v : intoViews) {
            c.into(v);
        }
    }

    private static BitmapRequestBuilder load(final Context context, final String url) {
        if (context instanceof FragmentActivity) {
            return loadImpl(Glide.with((FragmentActivity) context), url);
        }
        if (context instanceof Activity) {
            return loadImpl(Glide.with((Activity) context), url);
        }
        return loadImpl(Glide.with(context.getApplicationContext()), url);
    }

    private static BitmapRequestBuilder loadImpl(final RequestManager glide, final String url) {
        return glide.load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate();
    }

    private static String formatUrl(final String urlConstant, final String ownerID) {
        return String.format(urlConstant, "https://image.eveonline.com", ownerID);
    }

    private static BitmapRequestBuilder iconify(final Context context, final String url) {
        return load(context, url).transform(new CircleTransform(context)).fitCenter();
    }
}