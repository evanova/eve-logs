package com.tlabs.android.evelogs.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tlabs.android.evelogs.R;
import com.tlabs.android.evelogs.data.Channel;
import com.tlabs.android.evelogs.data.ChatData;
import com.tlabs.android.evelogs.data.Message;

import org.apache.commons.lang.StringUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageHolder> {

    static class MessageHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.rowTitle)
        TextView titleView;

        @Bind(R.id.rowText)
        TextView textView;

        @Bind(R.id.rowTime)
        TextView timeView;

        @Bind(R.id.rowImage)
        ImageView imageView;

        public MessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void renderMessage(final Channel channel, final Message message) {
            //FIXME need name->id conversion for icons
            String title = message.getAuthor();
            if (StringUtils.isBlank(title)) {
                title="?";
            }
            else if (title.length() == 1) {
                title = title.substring(0, 1);
            }
            else {
                title = title.substring(0, 2);
            }
            final int dpInPixels = (int)(48 * imageView.getResources().getDisplayMetrics().density);
            imageView.setImageDrawable(
                    TextDrawable.builder().
                            beginConfig().
                            fontSize(14).
                            width(dpInPixels).
                            height(dpInPixels).endConfig().
                            buildRound(title.toUpperCase(), ColorGenerator.MATERIAL.getColor(title)));
            this.titleView.setText(message.getAuthor());
            this.textView.setText(message.getText());
        }
    }

    private final List<ChatData> messages;
    private final Channel channel;

    public MessagesAdapter(final List<ChatData> messages) {
        this(null, messages);
    }

    public MessagesAdapter(final Channel channel, final List<ChatData> messages) {
        this.messages = messages;
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_channel, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        holder.renderMessage(this.channel, this.messages.get(position).getMessage());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.messages.size();
    }
}
