package com.tlabs.android.evelogs;

import com.squareup.leakcanary.LeakCanary;

public class ChatLogApplication extends android.app.Application {
    private static ChatLogComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
        component =
                DaggerChatLogComponent.builder().
                        chatLogModule(new ChatLogModule(this)).
                        build();
    }

    public static ChatLogComponent getContent() {
        return component;
    }
}
