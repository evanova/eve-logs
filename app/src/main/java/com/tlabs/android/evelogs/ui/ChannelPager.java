package com.tlabs.android.evelogs.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.tlabs.android.evelogs.R;
import com.tlabs.android.evelogs.data.Channel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rx.Subscription;

public class ChannelPager extends Fragment {

    public static class ChannelFragmentPagerAdapter extends FragmentStatePagerAdapter {

        private final List<ChannelFragment> fragments;
        private final Map<String, ChannelFragment> channeled;

        private Subscription subscription = null;

        protected ChannelFragmentPagerAdapter(final FragmentManager fm) {
            super(fm);
            this.fragments = new LinkedList<>();
            this.channeled = new HashMap<>();
        }

        @Override
        public int getItemPosition(Object object) {
            final int index = fragments.indexOf(object);
            if (index < 0) {
                return POSITION_NONE;
            }
            return POSITION_UNCHANGED;
        }

        @Override
        public final Fragment getItem(int position) {
            if (this.fragments.isEmpty()) {
                return null;
            }
            return this.fragments.get(position);
        }

        @Override
        public final int getCount() {
            return this.fragments.size();
        }

        @Override
        public final CharSequence getPageTitle(int position) {
            return this.fragments.get(position).getTitle();
        }

        public void setChannels(final ChatChannels channels) {
            if (null != subscription) {
                subscription.unsubscribe();
            }

            this.fragments.clear();

            for (Channel c: channels.getChannels()) {
                final ChannelFragment f = new ChannelFragment();
                f.setAdapter(channels.getMessagesAdapter(c.getId()));
                this.fragments.add(f);
                this.channeled.put(c.getId(), f);
            }
            this.subscription = channels.observable().subscribe(chat -> {
                ChannelFragment f = this.channeled.get(chat.getChannel().getId());
                if (null == f) {
                    f = new ChannelFragment();
                    f.setAdapter(channels.getMessagesAdapter(chat.getChannel().getId()));
                    this.fragments.add(f);
                    this.channeled.put(chat.getChannel().getId(), f);
                    notifyDataSetChanged();
                }
            });
            notifyDataSetChanged();
        }
    }

    private ViewPager viewPager;
    private ChannelFragmentPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adapter = new ChannelFragmentPagerAdapter(getChildFragmentManager());
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saved) {
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.f_pager, null);

        this.viewPager = (ViewPager)view.findViewById(R.id.fragmentPagerView);
        this.viewPager.setId(R.id.pagerView);
        this.viewPager.setOffscreenPageLimit(6);
        this.viewPager.setAdapter(this.adapter);

        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip)view.findViewById(R.id.fragmentPagerTabStrip);
        tabs.setViewPager(this.viewPager);

        return view;
    }

    public void setChannels(final ChatChannels channels) {
        this.adapter.setChannels(channels);
    }
}
