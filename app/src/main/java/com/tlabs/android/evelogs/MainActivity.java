package com.tlabs.android.evelogs;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.tlabs.android.evelogs.data.ChatData;
import com.tlabs.android.evelogs.ui.ChannelPager;
import com.tlabs.android.evelogs.ui.ChatChannels;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity {

    @Inject
    Observable<ChatData> observable;

    private Subscription subscription = null;
    private ChatChannels channels = new ChatChannels();

    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ChatLogApplication.getContent().inject(this);

        final RecyclerView listView = (RecyclerView)findViewById(R.id.activityDrawerRecycler);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setAdapter(this.channels.getChannelsAdapter());

        final ChannelPager pager = (ChannelPager)getSupportFragmentManager().findFragmentById(R.id.activityViewPager);
        pager.setChannels(this.channels);

        final DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
               // getActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
            }
        };
        drawer.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
        subscribe();
    }

    @Override
    protected void onStop() {
        unsubscribe();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unsubscribe();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_settings: {
                final Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void subscribe() {
        if (null == this.subscription) {
            this.subscription =
                    observable.
                            observeOn(AndroidSchedulers.mainThread()).
                            subscribe(new Subscriber<ChatData>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e(e.getLocalizedMessage(), e);
                                }

                                @Override
                                public void onNext(ChatData chatData) {
                                    channels.onReceive(chatData);
                                }
                            });
        }
    }

    private void unsubscribe() {
        if (null != this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }
}
