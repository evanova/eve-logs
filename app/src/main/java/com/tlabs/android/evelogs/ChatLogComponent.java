package com.tlabs.android.evelogs;


import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ChatLogModule.class})
public interface ChatLogComponent {

    void inject(MainActivity activity);
}
