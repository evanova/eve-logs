package com.tlabs.android.evelogs.ui;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tlabs.android.evelogs.R;
import com.tlabs.android.evelogs.content.Images;
import com.tlabs.android.evelogs.data.Channel;
import com.tlabs.android.evelogs.data.ChatData;
import com.tlabs.android.evelogs.data.Message;
import com.tlabs.android.evelogs.util.FormatHelper;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.ChannelHolder> {

    static class ChannelHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.rowTitle)
        TextView titleView;

        @Bind(R.id.rowText)
        TextView textView;

        @Bind(R.id.rowTime)
        TextView timeView;

        @Bind(R.id.rowImage)
        ImageView imageView;

        public ChannelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void renderChannel(final Channel channel, final Message last) {
            renderChanneled(channel);
            renderChanneled(channel, last);
        }

        private void renderChanneled(final Channel channel) {
            this.titleView.setText(channel.getName());

            if (StringUtils.startsWith(channel.getId(), "allianceid")) {
                Images.loadAllianceIcon(StringUtils.removeStart(channel.getId(), "allianceid").trim(), imageView);
                return;
            }
            if (StringUtils.startsWith(channel.getId(), "corpid")) {
                Images.loadCorporationIcon(StringUtils.removeStart(channel.getId(), "corpid").trim(), imageView);
                return;
            }

            if (StringUtils.startsWith(channel.getId(), "solarsystemid2")) {
                imageView.setImageResource(R.drawable.planets);//local chat
                return;
            }

            String title = channel.getName();
            switch (title.length()) {
                case 0:
                    title = "?";
                    break;
                case 1:
                    title = title.substring(0, 1);
                    break;
                default:
                    title = title.substring(0, 2);
            }
            imageView.setImageDrawable(
                    TextDrawable.builder().buildRound(title.toUpperCase(),
                            ColorGenerator.MATERIAL.getColor(title)));
        }

        private void renderChanneled(final Channel channel, final Message last) {
            if (null == last) {
                this.timeView.setVisibility(View.INVISIBLE);
                this.textView.setVisibility(View.INVISIBLE);
                return;
            }

            this.timeView.setVisibility(View.VISIBLE);
            this.textView.setVisibility(View.VISIBLE);

            this.textView.setText(last.getAuthor() + " > " + last.getText());
            Spannable spannable = (Spannable) textView.getText();
            spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, last.getAuthor().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            this.timeView.setText(FormatHelper.Time.SHORT(last.getDate()));
        }
    }

    private final List<Channel> channels;
    private final Map<String, Message> last;

    public ChannelsAdapter(final List<Channel> channels) {
        this.last = new HashMap<>();
        this.channels = channels;
        setHasStableIds(true);
    }

    @Override
    public ChannelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_channel, parent, false);
        return new ChannelHolder(view);
    }

    @Override
    public void onBindViewHolder(ChannelHolder holder, int position) {
        final Channel channel = this.channels.get(position);
        holder.renderChannel(channel, this.last.get(channel.getId()));
    }

    @Override
    public long getItemId(int position) {
        return this.channels.get(position).getId().hashCode();
    }

    @Override
    public int getItemCount() {
        return this.channels.size();
    }

    protected void notifyDataReceived(final ChatData data) {
        this.last.put(data.getChannel().getId(), data.getMessage());
        notifyItemChanged(indexOf(data.getChannel().getId()));
    }

    private int indexOf(final String channelId) {
        for (int i = 0; i < this.channels.size(); i++) {
            if (this.channels.get(i).getId().equals(channelId)) {
                return i;
            }
        }
        return -1;
    }
}
