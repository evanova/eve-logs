package com.tlabs.android.evelogs.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Channel {
    @JsonProperty
    private String id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String listener;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getListener() {
        return listener;
    }
}
