package com.tlabs.android.evelogs.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tlabs.android.evelogs.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChannelFragment extends Fragment {

    private RecyclerView.Adapter<?> adapter;
    private String title = "";

    @Bind(R.id.channelRecyclerView)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.f_channel, null);
        ButterKnife.bind(this, view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void setAdapter(final MessagesAdapter adapter) {
        this.adapter = adapter;
        this.title = (null == adapter.getChannel()) ? "All" : adapter.getChannel().getName();
    }

    public String getTitle() {
        return title;
    }
}
