##Chat In Local##

An Android application to display Eve chats on, well, an Android device obviously.

### Setup ###

You will need Gradle and Android Studio.


### Run ###

Nope. Not yet. Well, it shows stuff.


### Design Considerations ###

* Material design is good.

* Avoid coding; use RetroLambda, RxJava, RxAndroid, ButterKnife, Dagger2, Glide.

* Use RecyclerView(s) as much as possible. When not using a RecyclerView, apply the Holder pattern anyway.
