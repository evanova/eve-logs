package com.tlabs.eve.logs.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Message {
    public static class DateDeserializer extends JsonDeserializer<Long> {
        @Override
        public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            final SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:s");

            try {
                return format.parse(p.getText()).getTime();
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @JsonProperty("name")
    private String author;

    @JsonProperty
    private String text;

    @JsonProperty
    @JsonDeserialize(using = DateDeserializer.class)
    private long date;

    public String getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }

    public long getDate() {
        return date;
    }
}
