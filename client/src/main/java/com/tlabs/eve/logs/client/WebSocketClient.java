package com.tlabs.eve.logs.client;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;
import de.tavendo.autobahn.WebSocketOptions;


public class WebSocketClient<T> implements Closeable {
    private static final Logger LOG = LoggerFactory.getLogger(WebSocketClient.class);

    private static final ObjectMapper MAPPER;

    static {
        MAPPER = new ObjectMapper();
        MAPPER.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
    }

    private final WebSocketConnection connection = new WebSocketConnection();
    private final WebSocketHandler handler = new WebSocketHandler() {
        @Override
        public void onClose(int code, String reason) {
            super.onClose(code, reason);
        }

        @Override
        public void onTextMessage(String payload) {
            try {
                WebSocketClient.this.onMessage(MAPPER.readValue(payload, tClass));
            }
            catch (IOException e) {
                WebSocketClient.this.onError(e);
            }
        }

        @Override
        public void onRawTextMessage(byte[] payload) {
            try {
                WebSocketClient.this.onMessage(MAPPER.readValue(payload, tClass));
            }
            catch (IOException e) {
                WebSocketClient.this.onError(e);
            }
        }

        @Override
        public void onBinaryMessage(byte[] payload) {
            LOG.error("onBinaryMessage");
        }
    };

    private final Class<T> tClass;

    public WebSocketClient(final Class<T> tClass) {
        this.tClass = tClass;
    }

    public void connect(final String uri) throws IOException {
        if (this.connection.isConnected()) {
            LOG.warn("Connect: already connected");
            return;
        }
        try {
            final WebSocketOptions options = new WebSocketOptions();
            options.setSocketConnectTimeout(30000);
            options.setSocketReceiveTimeout(10 * 60 * 1000);
            options.setMaskClientFrames(true);
            connection.connect(uri,this.handler, options);
        }
        catch (WebSocketException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    public boolean isConnected() {
        return this.connection.isConnected();
    }

    @Override
    public void close() throws IOException {
        if (this.connection.isConnected()) {
            this.connection.disconnect();
        }
    }

    protected void onMessage(final T data) {}

    protected void onError(final Throwable t) {}
}
