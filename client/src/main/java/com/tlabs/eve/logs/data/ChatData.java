package com.tlabs.eve.logs.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatData {

    @JsonProperty
    private Channel channel;

    @JsonProperty
    private Message message;

    public Channel getChannel() {
        return channel;
    }

    public Message getMessage() {
        return message;
    }
}
