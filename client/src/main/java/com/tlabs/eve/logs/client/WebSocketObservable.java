package com.tlabs.eve.logs.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public class WebSocketObservable<T> {
    private static final Logger LOG = LoggerFactory.getLogger(WebSocketObservable.class);

    private WebSocketClient<T> ws;
    private String uri = "ws://192.168.1.1:9090/eve/chat";

    private final Subject<T, T> subject = BehaviorSubject.create();
    private final Observable<T> observable;

    public WebSocketObservable(Class<T> tClass) {
        this.ws = new WebSocketClient<T>(tClass) {
            @Override
            protected void onMessage(T data) {
                subject.onNext(data);
            }

            @Override
            protected void onError(Throwable t) {
                LOG.debug(t.getLocalizedMessage(), t);
                LOG.error(t.getLocalizedMessage());
            }
        };
        this.observable = this.subject.doOnSubscribe(new Action0() {
            @Override
            public void call() {
                if (!ws.isConnected()) {
                    try {
                        ws.connect(uri);
                    } catch (IOException e) {
                        LOG.error(e.getLocalizedMessage());
                    }
                }
            }
        }).
        doOnError(new Action1<Throwable>() {
            @Override
            public void call(Throwable t) {
                LOG.error(t.getLocalizedMessage(), t);
            }
        }).
        subscribeOn(Schedulers.io()).share();
    }

    public Observable<T> getObservable() {
        return observable;
    }

    public final void setUri(final String uri) {
        this.uri = uri;
        try {
            if (this.ws.isConnected()) {
                this.ws.close();
            }
            if (subject.hasObservers()) {
                this.ws.connect(uri);
            }
        }
        catch (IOException e) {
            LOG.error(e.getLocalizedMessage());
        }
    }
}
