# Eve Online Chat Logs WebSocket Server & Clients #

A Websocket server that emits messages when a message is receive in an Eve chat log. 

This is useful if you want to display chat logs outside the game using a web browser or an Android application or else.


### Setup ###

* You need to install the Java JDK 1.7 or 1.8 - sorry no Python, only Java :-)

* The server needs to be able to access the Eve chats logs directory, which is on the same machine as the Eve client.

* To setup the bind address of the server, check the `build.gradle` file under the 'ws' project and edit the run arguments.

* Run with `gradlew run`

* Using your preferred Web Socket client, you can now connect to `ws://your host:your port/eve/chat'. When chat logs change, a new JSON message will be received by your client.

* Start some small talk in local. See the results on your Websocket client (and on your corp mates faces).

* You can check the server health using `/eve/ping`. You will receive a **pong** reply each time you send a request to that endpoint.

### Clients ###

Any websocket client should work. 
"Simple WebSocket Client" works well on Chrome.

####Android Application####

Since the idea is to remove the screen clutter in the Eve Online client, the "app" sub-project contains a skeleton of an Android application as a proof of concept.


### Design Considerations ###

#### Websocket server ####

The server application is a runnable [Apache Camel](https://camel.apache.org/) application which watches over the chat log directory and dispatch changes to the websocket server that it started.

#####Routing:######

* A route is configured to watch over the chat log directory and create *topics* (which are also Camel routes) on the fly according to log file changes. Each *topic* represents a chat channel (the tabs you see on your Eve Online client) and is linked to a specific set of chat logs.

* Each time a new line is added to a chat log, it gets routed to the appropriate *topic*.

* When a message is routed to a *topic*, it is filtered, curated and transformed into a JSON representation. The resulting message is then routed to a websocket endpoint  which dispatches it to clients.

#####JSON message#####

The following is a sample message sent to clients (in which Evanova Android breaks all conventions by talking in  local).
```
{
  "channel": { 
    "id": "solarsystemid2 30001378",
    "name": "Local", 
    "listener": "Evanova Android" <-- The logged in capsuleer
  },
  "message": {
    "date": "2015.10.03 16:52:59",
    "name": "Evanova Android", <-- the author of the message
    "text": "Greetings"
  }
}
```

### Contribution ###

This project is fully opened. Feel free to make it better and creating pull requests or else.

If you would like to contribute on the Android client for chat logs or build some cool web tools around this, please let yourself known.

### Who do I talk to? ###

Evanova (evanova.mobile@gmail.com)